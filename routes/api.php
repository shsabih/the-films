<?php

use Illuminate\Support\Facades\Route;

// public routes
Route::get('/', function () {
    die(json_encode([
        'version' => '1.0',
        'message' => 'No direct access'
    ]));
});


Route::prefix('auth')->group(function () {
    Route::post('login', 'API\AuthController@login');
    Route::post('register', 'API\UsersController@register');
    Route::post('token-verify', 'API\AuthController@tokenCheck');
    Route::middleware('auth:api')->group(function () {
        Route::post('logout', 'API\AuthController@logout');
    });
});

Route::get('get-all-films', 'API\FilmsController@index');
Route::get('get-film/{id}', 'API\FilmsController@show');
Route::post('post-film-comment', 'API\FilmCommentsController@store')->middleware('auth:api');
Route::middleware('auth:api')->group(function () {

});
