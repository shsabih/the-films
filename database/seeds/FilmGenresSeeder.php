<?php

use App\Models\FileGenre;
use Illuminate\Database\Seeder;

class FilmGenresSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        FileGenre::create([
            'title' => 'Adventure',
            'film_id' => 1,
        ]);

        FileGenre::create([
            'title' => 'Sci-Fy',
            'film_id' => 1,
        ]);

        FileGenre::create([
            'title' => 'Action',
            'film_id' => 2,
        ]);

        FileGenre::create([
            'title' => 'Adventure',
            'film_id' => 2,
        ]);

        FileGenre::create([
            'title' => 'Action',
            'film_id' => 3,
        ]);

        FileGenre::create([
            'title' => 'Adventure',
            'film_id' => 3,
        ]);

        FileGenre::create([
            'title' => 'Comedy',
            'film_id' => 3,
        ]);
    }
}
