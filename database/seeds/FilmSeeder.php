<?php

use App\Models\Film;
use Illuminate\Database\Seeder;

class FilmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Film::create([
            'name' => 'Jumanji',
            'desc' => 'A must watch film',
            'release_date' => '2020-02-12',
            'rating' => '4',
            'ticket_price' => '20.50',
            'country' => 'USA',
            'photo' => 'uploads/JumanjiTheNextLevelTeaserPoster.jpg',
        ]);

        Film::create([
            'name' => 'Inception',
            'desc' => 'Inception is a must watch film!',
            'release_date' => '2019-03-21',
            'rating' => '5',
            'ticket_price' => '30.00',
            'country' => 'UK',
            'photo' => 'uploads/inception.jpg',
        ]);

        Film::create([
            'name' => 'Tenet',
            'desc' => 'Tenet is a reversed movie!',
            'release_date' => '2021-02-01',
            'rating' => '4',
            'ticket_price' => '25.00',
            'country' => 'USA',
            'photo' => 'uploads/Tenet_movie_poster.jpg',
        ]);
    }
}
