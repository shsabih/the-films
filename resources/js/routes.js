import Vue from "vue";
import VueRouter from 'vue-router'
import Dashboard from './components/Dashboard/Dashboard.vue';
import Auth from './components/Auth/index.vue';
import PageNotFound from "./components/PageNotFound";
import DetailPage from "./components/Dashboard/DetailPage";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    base: window.base_url,
    routes: [
        {
            path: '/login',
            name: 'login',
            component: Auth,
            meta: {
                guest: true
            }
        },
        {
            path: '/',
            name: 'dashboard',
            component: Dashboard
        },
        {
            path: '/:id',
            name: 'detail-page',
            component: DetailPage,
        },
        {
            path: '/auth',
            name: 'auth',
            component: Auth,
            meta: {
                guest: true
            }
        },
        { // 404 page
            path: '*',
            name: 'not-found',
            component: PageNotFound,
        },
    ]
});

router.beforeEach(async (to, from, next) => {
    let token = Vue.$cookies.get('lv_token')

    if (to.name === 'login') {
        if(token == null) {
            next()
        }
        else{
            next({name: 'dashboard'})
        }
    }
    next()
})

export default router;
