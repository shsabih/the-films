require('./bootstrap');

window.base_url = document.querySelector('meta[name~="base_url"]').content;
window.api_url = document.querySelector('meta[name~="api_url"]').content;
window.csrf = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

import Vue from 'vue';
import App from './components/App.vue';
import router from './routes';
import store from './store/store'
import 'vuex-toast/dist/vuex-toast.css'
import {Toast} from 'vuex-toast'
import VueCookies from 'vue-cookies'
import SimpleVueValidator from 'simple-vue-validator'

Vue.config.productionTip = false;
Vue.use(VueCookies);
Vue.use(SimpleVueValidator);

Vue.component('toast', Toast);

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
