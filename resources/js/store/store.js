import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
import AuthLogin from './modules/Auth/Login';

import {ADD_TOAST_MESSAGE, createModule} from 'vuex-toast'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        AuthLogin,
        toast: createModule({
            dismissInterval: 5000
        })
    },
    state: {
        films: {},
        film: {
            comments: [],
        }
    },
    actions: {
        fetchFilms({commit}) {
            axios.get(api_url + '/get-all-films').then(resp => {
                if( resp.data.bool ) {
                    commit('setFilms', resp.data.result)
                }
            });
        },
        fetchFilm({commit}, payload) {
            axios.get(api_url + '/get-film/' + payload).then(resp => {
                if( resp.data.bool ) {
                    commit('setFilm', resp.data.result)
                }
            });
        },
        storeComments({commit, dispatch}, payload) {
            let token = Vue.$cookies.get('lv_token')
            const config = {
                headers: {
                    'Authorization': 'Bearer ' +token
                }
            }
            axios.post(api_url + '/post-film-comment', payload, config).then(resp => {
                dispatch('fetchFilm', payload.film_id)
                dispatch('successNoty', 'Comment posted successfully!')
            })
        },
        successNoty({dispatch}, msg) {
            dispatch(ADD_TOAST_MESSAGE, {text: msg, type: 'success'})
        },
        infoNoty({dispatch}, msg) {
            dispatch(ADD_TOAST_MESSAGE, {text: msg, type: 'info'})
        },
        errorNoty({dispatch}, msg) {
            dispatch(ADD_TOAST_MESSAGE, {text: msg, type: 'danger'})
        },
    },
    getters: {
        getFilms: (state) => {
            return state.films
        },
        getFilm: (state) => {
            return state.film
        }
    },
    mutations: {
        setFilms(state, payload) {
            state.films = payload;
        },
        setFilm(state, payload) {
            state.film = payload;
        }
    }
});
