import axios from 'axios';

export default {
    state: {
        users: {
            data: []
        },
        roles: null,
        status: {
            0: 'Incomplete',
            1: 'Approved',
            2: 'Pending',
            3: 'Deactivate'
        },
        userStatusCount: {incomplete: 0, pending: 0}
    },
    actions: {
        searchUser({commit}, post) {
            axios.post(api_url + '/search-users?page=' + post.page, post).then(resp => {
                commit('setUsers', resp.data.result)
            })
        },
        userRole({commit}) {
            axios.get(api_url + '/user-roles').then(resp => {
                commit('setRoles', resp.data.result)
            })
        },
        changeUserStatus({commit, dispatch}, post) {
            axios.post(api_url + '/update-user-status', post).then(resp => {
                commit('setUsers', resp.data.result)
                dispatch('callUserStatusCount');
                dispatch('successNoty', 'Status changed successfully!');
            })
        },
        async callUserStatusCount({commit}) {
            let counts = await axios.get(api_url + '/user-counts')
            if (counts.data.bool)
                commit('setUserStatusCount', counts.data.result)
        }
    },
    getters: {
        getUsersData: (state) => {
            return state.users;
        },
        getRoleData: (state) => {
            return state.roles;
        },
        getStatus: (state) => {
            return role_ind => {
                return state.status[role_ind]
            }
        },
        getUserStatusCount: (state) => {
            return state.userStatusCount
        }
    },
    mutations: {
        setUsers(state, payload) {
            state.users = payload;
        },
        setRoles(state, payload) {
            state.roles = payload;
        },
        setUserStatusCount(state, payload) {
            state.userStatusCount = payload;
        }
    }
}
