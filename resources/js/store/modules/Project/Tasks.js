import axios from 'axios';

export default {
    state: {
        tasks: {
            data: []
        }
    },
    actions: {
        searchTasks({commit}, post) {
            axios.post(api_url+'/search-tasks?page='+post.page, post).then(resp => {
                commit('setTasks', resp.data.result)
            })
        },
        completeTask({commit, dispatch}, post) {
            axios.post(api_url+'/complete-tasks', post).then(resp => {
                let output = resp.data;

                let status = 'completed';
                if( post.status === '1' ) {
                    status = 'reopend';
                }
                else if( post.status === '2' && typeof post.reopened !== 'undefined' && post.reopened === '1' ) {
                    status = 'reopened';
                }
                else if( post.status === '2' ) {
                    status = 'accepted';
                }
                else if( post.status === '3' ) {
                    status = 'declined';
                }
                else if( post.status === '5' ) {
                    status = 'closed';
                }

                dispatch('successNoty', 'Task '+status+' successfully!');

                if( typeof post.project_id !== 'undefined' || typeof post.decline_desc !== 'undefined' ) {
                    dispatch('project', post.project_id)
                    return true;
                }

                if( typeof output.bool !== 'undefined' ) {
                    commit('setTasks', output.result)
                    return true;
                }

                dispatch('errorNoty', output.message);
            })
        },
        changeStatus({commit, dispatch}, post) {
            axios.post(api_url+'/change-status-tasks', post).then(resp => {
                let output = resp.data;
                if( output.bool ) {
                    dispatch('successNoty', 'Status changed successfully!');
                    commit('setTasks', output.result)
                    return true;
                }
                dispatch('errorNoty', output.message);
            })
        },
        saveTaskNotes({commit, dispatch}, post) {
            axios.post(api_url + '/add-supervisor-task-notes', post).then(() => {
                dispatch('successNoty', 'Notes saved successfully!');
                dispatch('project', post.project_id)
            })
        },
        async deleteTasks({commit, dispatch}, post) {
            await axios.post(api_url+'/delete-tasks', post).then(resp => {
                let output = resp.data;
                if( output.bool ) {
                    dispatch('successNoty', 'Task(s) deleted successfully!');
                    return true;
                }
                dispatch('errorNoty', output.message);
            })
        },
        async attachNotes({commit, dispatch}, post) {
            const config = {
                headers: {
                    'content-type': 'multipart/form-data'
                }
            }
            await axios.post(api_url+'/attach-tasks-notes', post, config).then(resp => {
                console.log(resp)
                let output = resp.data;
                if( output.bool ) {
                    dispatch('successNoty', 'Task completed successfully!');
                    commit('setTasks', output.result)
                    return true;
                }
                dispatch('errorNoty', output.message);
            })
        }
    },
    getters: {
        getTasksData: (state) => {
            return state.tasks;
        }
    },
    mutations: {
        setTasks(state, payload) {
            state.tasks = payload;
        }
    }
}
