import axios from 'axios';

export default {
    state: {
        projects: {
            data: []
        },
        propertyType: null,
        phaselineSummary: null,
        projectPerPage: {text: '10', value: '10'},
        userUnreadNotifications: 0,
        userNotifications: []
    },
    actions: {
        phaselineSummary({commit}) {
            axios.get(api_url + '/phase-summary').then(resp => {
                commit('setPhaselineSummary', resp.data.result)
            })
        },
        propertyTypeSummary({commit}) {
            axios.get(api_url + '/property-type').then(resp => {
                commit('setPropertyType', resp.data.result)
            })
        },
        searchProject({commit}, post) {
            axios.post(api_url + '/search-projects?page=' + post.page, post).then(resp => {
                commit('setProjects', resp.data)
            })
        },
        duplicateProject({commit, dispatch}, post) {
            dispatch('infoNoty', 'Project duplicating. Please wait..');
            axios.post(api_url + '/duplicate-project', post).then(resp => {
                let output = resp.data;
                if (output.bool) {
                    dispatch('successNoty', 'Project duplicated successfully!');
                    commit('setProjects', resp.data)
                    return true;
                }
                dispatch('errorNoty', output.message);
            })
        },
        deleteProject({commit, dispatch}, post) {
            axios.post(api_url + '/delete-project', post).then(resp => {
                let output = resp.data;
                if (output.bool) {
                    dispatch('successNoty', 'Project deleted successfully!');
                    commit('setProjects', resp.data)
                    return true;
                }
                dispatch('errorNoty', output.message);
            })
        },
        getNoty({commit, dispatch}) {
            axios.get(api_url + '/get-notifications').then(resp => {
                let output = resp.data;
                if (output.bool) {
                    commit('setUserNotifications', resp.data.result)
                    return true;
                }
                dispatch('errorNoty', output.message);
            })
        },
        getUnreadNoty({commit, dispatch}) {
            axios.get(api_url + '/get-unread-notifications').then(resp => {
                let output = resp.data;
                if (output.bool) {
                    commit('setUnreadUserNotifications', resp.data.result)
                    return true;
                }
                dispatch('errorNoty', output.message);
            })
        }
    },
    getters: {
        getProjectData: (state) => {
            return state.projects
        },
        getPhaselineSummary: (state) => {
            return state.phaselineSummary
        },
        getPropertyType: (state) => {
            return state.propertyType
        },
        getProjectPerPage: (state) => {
            return state.projectPerPage
        },
        getUserNotifications: (state) => {
            return state.userNotifications
        },
        getUnreadUserNotifications: (state) => {
            return state.userUnreadNotifications
        }
    },
    mutations: {
        setProjects(state, payload) {
            state.projects = payload.result;
        },
        setPhaselineSummary(state, payload) {
            state.phaselineSummary = payload;
        },
        setPropertyType(state, payload) {
            state.propertyType = payload
        },
        updateProjectPerPage(state, payload) {
            state.projectPerPage = payload
        },
        setUserNotifications(state, payload) {
            state.userNotifications = payload
        },
        setUnreadUserNotifications(state, payload) {
            state.userUnreadNotifications = payload
        }
    }
}
