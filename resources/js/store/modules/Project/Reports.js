import axios from 'axios';

export default {
    state: {
        tasks: [],
        projects: [],
        users: [],
        payments: [],
    },
    actions: {
        tasksDoughnut({commit}) {
            axios.get(api_url + '/get-tasks-count').then(resp => {
                commit('setTasksDoughnut', resp.data)
            })
        },
        projectsDoughnut({commit}) {
            axios.get(api_url + '/get-projects-count').then(resp => {
                commit('setProjectsDoughnut', resp.data)
            })
        },
        usersDoughnut({commit}) {
            axios.get(api_url + '/get-users-count').then(resp => {
                commit('setUsersDoughnut', resp.data)
            })
        },
        paymentDoughnut({commit}) {
            axios.get(api_url + '/get-payment-count').then(resp => {
                commit('setPaymentDoughnut', resp.data)
            })
        }
    },
    getters: {
        getTasksDoughnut: (state) => {
            return state.tasks
        },
        getProjectsDoughnut: (state) => {
            return state.projects
        },
        getUsersDoughnut: (state) => {
            return state.users
        },
        getPaymentDoughnut: (state) => {
            return state.payments
        },
    },
    mutations: {
        setTasksDoughnut(state, payload) {
            state.tasks = payload
        },
        setProjectsDoughnut(state, payload) {
            state.projects = payload
        },
        setUsersDoughnut(state, payload) {
            state.users = payload
        },
        setPaymentDoughnut(state, payload) {
            state.payments = payload
        }
    }
}
