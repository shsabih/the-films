export default {
    namespaced: true,
    state: {
        profile_photo: 'https://films.com/wp-content/uploads/2019/10/MicrosoftTeams-image__1_.png'
    },
    mutations: {
        setProfilePhoto(state, url) {
            if (url !== "")
                state.profile_photo = base_url + "/storage/uploads/" + url
            else
                state.profile_photo = 'https://films.com/wp-content/uploads/2019/10/MicrosoftTeams-image__1_.png'
        }
    }
}
