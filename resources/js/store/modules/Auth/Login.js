import Vue from 'vue'

export default {
    namespaced: true,
    state: {
        is_login: false,
        is_form_submit: false,
        error: null,
        success: null,
        user_roles: [],
        userToken: null
    },
    actions: {
        async tokenVerify({commit}) {
            let token = Vue.$cookies.get('lv_token')
            let _conf = {
                headers: {
                    Authorization: 'Bearer ' + token
                }
            }
            await axios.post(api_url + '/auth/token-verify', {}, _conf).then(resp => {
                if (resp.data.status) {
                    commit('setUToken', token)
                } else {
                    commit('setUToken', null)
                }
            })
        },
        logout({commit}) {
            commit('setFormStatus', true)
            axios.post(api_url + '/auth/logout').then(resp => {
                commit('setUToken', null)
            }).finally(() => {
                commit('setFormStatus', false)
            })
        },
        async loginUser({commit, dispatch}, payload) {
            commit('setFormStatus', true)
            commit('setError', null)
            commit('setSuccess', null)
            await axios.post(api_url + '/auth/login', payload).then(resp => {
                if (resp.data.bool) {
                    commit('setSuccess', "Successfully logged in.")
                    commit('setUToken', resp.data['access_token'])
                } else {
                    commit('setError', resp.data.message)
                }
            }).catch(err => {
                commit('setError', "Server error!")
            }).finally(() => {
                commit('setFormStatus', false)
            })
        }
    },
    getters: {
        isUserLogin: (state) => {
            return state.is_login
        },
        isFormSubmit: (state) => {
            return state.is_form_submit
        },
        getError: (state) => {
            return state.error
        },
        getSuccess: (state) => {
            return state.success
        },
        getUserRoles: (state) => {
            return state.user_roles
        }
    },
    mutations: {
        setFormStatus(state, payload) {
            state.is_form_submit = payload
        },
        setError(state, payload) {
            state.error = payload
        },
        setSuccess(state, payload) {
            state.success = payload
        },
        setUToken(state, payload) {
            if (payload == null) {
                Vue.$cookies.remove('lv_token')
                state.is_login = false
            } else {
                Vue.$cookies.set('lv_token', payload, '30d')
                state.is_login = true
            }
            state.userToken = payload
        },
    }
}
