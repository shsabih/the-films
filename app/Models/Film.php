<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Film extends Model
{
    protected $guarded = [];

    /**
     * Film Genres
     * @return HasMany
     */
    public function genres(): HasMany
    {
        return $this->hasMany(FileGenre::class, 'film_id', 'id');
    }

    /**
     * Film Comments Relation
     * @return HasMany
     */
    public function comments(): HasMany
    {
        return $this->hasMany(FilmComment::class, 'film_id', 'id');
    }
}
