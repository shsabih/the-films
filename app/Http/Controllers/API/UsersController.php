<?php

namespace App\Http\Controllers\API;

use App\Helpers\Emails;
use App\Http\Controllers\Controller;
use App\Jobs\JobRegisterUser;
use App\Models\Role;
use App\Models\UserProjectRole;
use App\Repositories\PhaselineRepository;
use App\Repositories\UserRepository;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class UsersController extends Controller
{
    /**
     * @var UserRepository
     */
    private $repo;

    /**
     * UsersController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->repo = $userRepository;
    }

    /**
     * Display a listing of users.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $users = $this->repo->all();
        return response()->json($users);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|min:3|max:255',
            'email' => 'required|min:6|unique:users,email|max:255',
            'password' => 'required|min:6|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'bool' => false,
                'message' => $validator->errors()->first()
            ]);
        }

        $data = [
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password'))
        ];

        if (!$this->repo->create($data)['bool']) {
            return response()->json([
                'bool' => false,
                'message' => "Server error!"
            ]);
        }

        return response()->json(AuthController::attemptLogin($request, $request->email, $request->password));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified user in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request): JsonResponse
    {
        $data = $request->except(['id', 'limit']);
        $data['status'] = $request->status['value'];

        $this->repo->update($data, $request->id);

        return $this->searchUser($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Search User API
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function searchUser(Request $request): JsonResponse
    {
        $filters = $request->all();
        if ($request->has('by_role')) {
            $roles = [];
            foreach ($request->by_role as $role)
                $roles[] = $role['value'];

            $filters['by_role'] = $roles;
        }

        $output = $this->repo->searchUser($filters);
        return response()->json($output);
    }
}
