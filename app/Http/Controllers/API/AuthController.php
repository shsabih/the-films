<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Login Request
     * @param Request $request
     * @param $email
     * @param $pass
     * @param bool $remember
     * @return array
     */
    public static function attemptLogin(Request $request, $email, $pass, $remember = false)
    {
        $credentials = [
            'email' => $email,
            'password' => $pass
        ];

        if (!Auth::attempt($credentials))
            return [
                'bool' => false,
                'message' => 'Wrong username or password. Please try again'
            ];
        $user = $request->user();

        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($remember)
            $token->expires_at = Carbon::now()->addWeeks(1);

        $token->save();

        return [
            'bool' => true,
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ];
    }

    /**
     * Login API Method
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
            'remember' => 'boolean'
        ]);

        return response()->json(self::attemptLogin($request, $request->email, $request->password, $request->remember));
    }

    /**
     * User Authentication (Token Verification) Check
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function tokenCheck(Request $request)
    {
        if( $authCheck = Auth::guard('api')->check() ) {
            return response()->json([
                'status' => $authCheck,
                'user' => Auth::guard('api')->user()
            ]);
        }

        return response()->json([
            'status' => $authCheck,
            'user' => Auth::guard('api')->user()
        ]);
    }

    /**
     * Logout User
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json([
            'status' => true
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
}
