<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Repositories\FilmCommentRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FilmCommentsController extends Controller
{
    /**
     * @var FilmCommentRepository
     */
    private $repo;

    /**
     * FilmCommentsController constructor.
     * @param FilmCommentRepository $commentRepository
     */
    public function __construct(FilmCommentRepository $commentRepository)
    {
        $this->repo = $commentRepository;
    }

    /**
     * Store the film comments
     * @param Request $request
     * @return array
     */
    public function store(Request $request): array
    {
        $data = $request->all();
        $data['user_id'] = Auth::guard('api')->user()->id;
        return $this->repo->create($data);
    }
}
