<?php

namespace App\Repositories;

use App\User;

class UserRepository implements RepositoryInterface
{
    /**
     * @var User
     */
    private $model;

    /**
     * UserRepository constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    /**
     * @return array
     */
    public function all()
    {
        try {
            $data = $this->model->get();
            return [
                'bool' => true,
                'result' => $data
            ];
        } catch (\Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @return array
     */
    public function create(array $data)
    {
        try {
            $data = $this->model->create($data);
            return [
                'bool' => true,
                'result' => $data
            ];
        } catch (\Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param array $data
     * @param $id
     * @return array|mixed
     */
    public function update(array $data, $id)
    {
        try {
            $output = $this->model->find($id)->update($data);

            return [
                'bool' => true,
                'message' => $output
            ];
        } catch (\Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        try {
            $output = $this->model->find($id)->delete();

            return [
                'bool' => true,
                'message' => $output
            ];
        } catch (\Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage()
            ];
        }
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        try {
            $data = $this->model->find($id);
            return [
                'bool' => true,
                'result' => $data
            ];
        } catch (\Exception $exception) {
            return [
                'bool' => false,
                'message' => $exception->getMessage(),
            ];
        }
    }
}
