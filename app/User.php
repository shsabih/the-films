<?php

namespace App;

use App\Models\ProjectPhaselineTask;
use App\Models\Role;
use App\Models\UserKeyValue;
use App\Models\UserProjectExperience;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'name', 'email', 'password', 'role_id', 'phone', 'is_finance', 'status', 'pass_check'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * User Project Roles
     * @return HasMany
     * \HasMany
     */
    public function projectRole(): HasMany
    {
        return $this->hasMany(Models\UserProjectRole::class, 'user_id', 'id')->with('role');
    }

    /**
     * @return BelongsToMany
     */
    public function projects(): BelongsToMany
    {
        return $this->belongsToMany(Models\UserProjectRole::class, 'user_project_roles', 'user_id', 'role_id');
    }

    /**
     * @return HasMany
     */
    public function userProjects(): HasMany
    {
        return $this->hasMany(Models\UserProjectRole::class);
    }

    /**
     * Phases belongs to User
     * @return BelongsToMany
     */
    public function phases(): BelongsToMany
    {
        return $this->belongsToMany(Models\ProjectPhaselineUser::class, 'project_phaseline_users', 'user_id', 'phase_id');
    }

    /**
     * @return HasMany
     */
    public function userPhases(): HasMany
    {
        return $this->hasMany(Models\ProjectPhaselineUser::class);
    }

    /**
     * User Main Role
     * @return BelongsTo
     */
    public function role(): BelongsTo
    {
        return $this->belongsTo(Models\Role::class);
    }

    /**
     * Assigned Tasks
     * @return HasMany
     */
    public function tasks(): HasMany
    {
        return $this->hasMany(ProjectPhaselineTask::class, 'user_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function metas(): HasMany
    {
        return $this->hasMany(UserKeyValue::class);
    }

    /**
     * @return HasOne
     */
    public function profilePhoto(): HasOne
    {
        return $this->hasOne(UserKeyValue::class)->where('key', 'profile_photo');
    }

    /**
     * @return HasMany
     */
    public function projectExperience(): HasMany
    {
        return $this->hasMany(UserProjectExperience::class);
    }
}
