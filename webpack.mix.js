const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    /*.sass('resources/sass/app.scss', 'public/css')
    .styles([
        'resources/css/dashboard.css',
        'resources/css/style.css',
        'resources/css/project_detail.css',
        // 'resources/css/select2.min.css',
        // 'resources/css/fancybox.min.css',
        'resources/css/font-awesome.min.css',

    ], 'public/css/films.css')
    .scripts([
        // 'resources/js/fancybox.min.js',
        // 'resources/js/form-field-selecthierarchical.min.js',
        // 'resources/js/select2.min.js',
    ], 'public/js/films.js')*/;
